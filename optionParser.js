module.exports = () => {
    let gameId, teamIds = [], playersPerTeam;
    for(let i=2; i<process.argv.length; i++) {
        const arg = process.argv[i];
        switch(arg) {
            case '--game':
                gameId = Number(process.argv[++i]);
                break;
            case '--teams':
                let teamId;
                for(i++; !isNaN(teamId = Number(process.argv[i])); i++) {
                    teamIds.push(teamId);
                }
                i--;
                break;
            case '--players':
                playersPerTeam = Number(process.argv[++i]);
                break;
            default:
                throw new Error("Unknown argument: "+arg);
        }
    }
    if(isNaN(gameId)) {
        throw new Error("Please enter a valid gameId after option --game");
    } else if(isNaN(playersPerTeam)) {
        throw new Error("Please enter a valid number of players per team after option --players");
    } else if(playersPerTeam>100) {
        throw new Error("Players per team is not allowed to be above 100");
    } else if(!teamIds.length) {
        throw new Error("Please enter atleast one valid teamId after option --teams (separate by <space> if more than 1 team)")
        
    }
    return {gameId, playersPerTeam, teamIds};
}
