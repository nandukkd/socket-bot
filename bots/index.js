const { io: ioClient } = require('socket.io-client')
const path = require('path')
const pino = require('pino')
const axios = require('axios')
const fs = require('fs/promises')
axios.defaults.baseURL = process.env.GAME_API_BASE_URL;

let timer = null;

let gameSocks = [];

const randomizer = (start, end) => Math.floor(Math.random()*end) + start

const addPlayerApi = async(name, gameId, team, abort) => {
    const formData = new FormData();
    formData.append("name", name);
    formData.append("game_id", gameId);
    formData.append("team_id", team);
    return await axios.post('/add-player', formData, {signal: abort.signal});
}

const delay = ms => new Promise(res => {
    timer = setTimeout(res, ms)
})

const answerQuestionApi = async(gameId, teamId, questionId, userId, selected, isRoundEnd, abort) => {
    const formData = new FormData();
    formData.append("game_id", gameId);
    formData.append("team_id", teamId);
    formData.append("question_id", questionId);
    formData.append("player_id", userId);
    formData.append("answer_ids", JSON.stringify(selected));
    formData.append("response_speed", 2);
    formData.append("is_round_end", isRoundEnd);
    return await axios.post('/save-question-answer', formData, {signal: abort.signal});
}

function kill(item) {
    const sockIndex = gameSocks.findIndex(i => i.item===item)
    if(sockIndex > -1) {
        const sock = gameSocks[sockIndex].sock;
        sock.offAny();
        sock.disconnect();
        gameSocks.splice(sockIndex, 1);
    }
}

function killAll() {
    if(timer) {
        clearTimeout(timer);
        timer = null;
    }
    let i;
    while(i = gameSocks.pop()) {
        i.sock.disconnect();
    }
}

async function handleError (error, id, message) {
}

const assignError = (error, id) => {
    error.gameId = id.gameId;
    error.teamId = id.teamId;
    error.userId = id.userId;
    error.item = id.item;
    error.index = id.index;
}


async function doIt({gameId, teamId, index}, item, abort, monitorListener, logger) {
    let userId, id = {gameId, teamId, index, key: item};
    try {
        monitorListener.emit('item', id)
        monitorListener.emit('action', id, 'starting') // now lets connect to socket
        logger.info(id, 'starting')
        const gameSock = ioClient(
            process.env.GAME_SOCKET_URL,// 'wss://admin.app.yakeety.com/',
            {
                //transports: ['websocket'],
            }
        );
        gameSocks.push({item, sock: gameSock});
        gameSock.on('connect_error', (error) => {
            assignError(error, id);
            logger.error(error, 'connect_error');
            monitorListener.emit('action', id, 'socket_error', {message: error.message, stack: error.stack})
        })
        gameSock.on('update_team', data => {
            //logger.info('update_team')
            //monitorListener.emit('action', id, 'update_team', {...data, sendTime: Date.now()})
        });
        gameSock.on('set_question', async(data) => {
            monitorListener.emit('action', id, 'player_screen_question', data);
            logger.info({...id, data}, 'set_question');
            try {
                await delay(randomizer(50, 10000))
                const answers = data.question.get_answers_by_question_id;
                const ans = answers[Math.floor(Math.random()*answers.length)].id
                monitorListener.emit('action', id, 'player_answer_pending', {questionId: data.question.id});
                const res = await answerQuestionApi(gameId, teamId, data.question.id, userId, [ans], false, abort);
                logger.info({status: res.status, id, questionId: data.question.id, answerId: ans}, 'question_answered');
                if(!res.data.success) {
                    const e = new Error("API was not successfull");
                    e.request = true;
                    e.response = res;
                    throw e;
                } else {
                    gameSock.emit('set_answered', { id: gameId, player_id: {id: userId} });
                    monitorListener.emit('action', id, 'player_answer_success', {res: res.data, questionId: data.question.id, answerId: ans});
                }
            } catch (error) {
                if(error.request) {
                    if(!axios.isCancel(error)) {
                        const e = new Error(error.message);
                        assignError(e, id)
                        e.data = error.response?.data;
                        e.status = error.response?.status;
                        logger.error(e, 'answer_failed')
                    }
                } else {
                    assignError(error, id)
                    logger.error(error, 'answer_failed')
                }
                //kill(item);
                monitorListener.emit('action', id, 'player_question_failed', {message: error.message, questionId: data.question.id})
            }
        })
        gameSock.on('disconnect', (reason, details) => {
            monitorListener.emit('action', id, 'socket_disconnected', {reason, details})
            logger.info({...id, reason, details}, 'socket_disconnected');
        })
        await new Promise((resolve) => {
            gameSock.on('connect', () => {
                logger.info(id, 'socket_connected');
                monitorListener.emit('action', id, 'socket_connected')
                gameSock.emit('join', gameId)
                resolve();
            })
        })
        let res;
        monitorListener.emit('action', id, 'api_called')
        res = await addPlayerApi(item, gameId, teamId, abort);
        if(!res.data.success) {
            kill(item)
            const e = new Error("Response showed that API failed")
            e.request = true;
            e.response = res;
            throw e;
        }
        logger.info({status: res.status, data: res.data, id}, 'api_responded');
        userId = res.data.data.id;
        id.userId = userId;
        monitorListener.emit('action', id, 'api_responded', res.data)
        gameSock.emit('add_team', { team_id: teamId, game_id: gameId, players: res.data.team_members });
    } catch (error) {
        if(error.request) {
            if(!axios.isCancel(error)) {
                const response = error.response;
                error.data = response?.data;
                error.status = response?.status;
                error.request = null;
                error.response = null;
                assignError(error, id);
                logger.error(error, 'api_error')
                monitorListener.emit(
                    'action', id, 'api_error',
                    {data: response?.data, status: response?.status, message: error.message},
                );
            }
        } else {
            assignError(error, id);
            logger.error(error, 'error')
            monitorListener.emit('action', id, 'error', error)
        }
    }
}

const logName = () => {
    const now = new Date();
    const actualNow = new Date(now.getTime()-now.getTimezoneOffset()*60000);
    const str = actualNow.toISOString();
    return str.replace('Z', '')+'.log';
}

module.exports = function(listener) {
    let abort, logger;

    const stopAll = () => {
        logger?.info('stopping')
        abort?.abort();
        abort = null;
        killAll();
        logger?.info('stopped')
        logger = null;
        listener?.emit('stopped')
    }
    listener.on('stop', stopAll);
    listener.on('start', async(teams) => {
        console.log('starting', teams)
        if(abort) {
            abort.abort();
            killAll();
        }
        const logPath = path.join(__dirname, '..', 'logs', logName());
        await fs.writeFile(logPath, '', 'utf8');
        logger = pino(pino.destination(logPath))
        abort = new AbortController();
        for(let {gameId, teamId, playersCount} of teams) {
            for(let i=0; i<playersCount; i++) {
                doIt({gameId, teamId, index: i}, gameId+'_'+teamId+'_item_'+i+'_'+Date.now(), abort, listener, logger);
                await delay(100);
            }
        }
    })
    return stopAll;
}
