require('dotenv').config();
const EventEmitter = require('node:events')
const v8 = require('v8')
const { Server } = require('socket.io')

let monitorListener, stopAll;

const parseBytes = (b, level=0) => {
    if(b > 1000)
        return parseBytes(b/1024, level+1)
    else
        return (Math.floor(b*100)/100)+' '+['','K','M','G','T','Z'][level]+'B'
}

const heapData = () => {
    const heap = v8.getHeapStatistics();
    return {
        max: parseBytes(heap.heap_size_limit),
        total: parseBytes(heap.total_heap_size),
        used: parseBytes(heap.used_heap_size)
    }
}

function monitor () {
    monitorTimer = setTimeout(() => {
        if(monitorListener) {
            monitorListener?.emit('memory', heapData())
        }
        monitor();
    }, 2000)
}


const setup = require('./bots')
const io = new Server({
    cors: true
})

io.on('connection', socket => {
    console.log('monitor connected')
    monitorListener = socket;
    stopAll = setup(monitorListener);
    socket.on('disconnect', () => {
        console.log('monitor disconnected')
        stopAll && stopAll();
    })
})

io.listen(process.env.BOT_SERVER_PORT);

process.on('SIGINT', () => {
    console.log('\nterminating peacefully...')
    io.close();
})
