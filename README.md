# Bot Tester for Yakeety


## Setup
### Installation
Install the dependencies first.
```
npm ci
```

### Configuration
First setup a `.env` file like this:
```
CORS_ORIGINS = "http://localhost:3000 http://localhost:3001"
BOT_SERVER_PORT = 4000
GAME_SOCKET_URL = ws://yakeety-socket-server.com
GAME_API_BASE_URL = http://yakeety-api-server.com/endpoint
```
Then run it.
## Getting Started
For running bots with status logging in the console itself:
```
npm run local -- --game {gameId} --players {playersPerTeam} --teams {teamId1} {teamId2} {teamId3}...
```

For running bots by connecting to a Monitor:
```
npm run listen
```

## Logs
For each run, a new log file will be created inside `logs` folder with date.
