require('dotenv').config();
const EventEmitter = require('node:events')
const v8 = require('v8')

let monitorListener;

let {gameId, teamIds, playersPerTeam} = require('./optionParser')();

const parseBytes = (b, level=0) => {
    if(b > 1000)
        return parseBytes(b/1024, level+1)
    else
        return (Math.floor(b*100)/100)+' '+['','K','M','G','T','Z'][level]+'B'
}

const heapData = () => {
    const heap = v8.getHeapStatistics();
    return {
        max: parseBytes(heap.heap_size_limit),
        total: parseBytes(heap.total_heap_size),
        used: parseBytes(heap.used_heap_size)
    }
}

function monitor () {
    monitorTimer = setTimeout(() => {
        if(monitorListener) {
            monitorListener?.emit('memory', heapData())
        }
        monitor();
    }, 2000)
}


const setup = require('./bots')
const start = () => {
    console.log('starting...')
    let bots = 0, sockets = 0, disconnections = 0;
    const joinApis = {pending: 0, success: 0, failed: 0};
    const questions = {};
    //const coun
    monitorListener = new EventEmitter();
    monitorListener.addListener('item', (item) => {
        //sockets.pending++;
    })
    const logQuestion = qid => {
        console.log(`QUESTION ${qid} REACHED ${questions[qid].asked} ANSWERED ${Object.values(questions[qid].answered).join('|')} FAILED ${questions[qid].failed}`);
    }
    monitorListener.addListener('action', (item, event, payload) => {
        switch(event) {
            case 'starting':
                bots++;
                break;
            case 'api_called':
                joinApis.pending++;
                break;
            case 'api_responded':
                joinApis.pending--;
                joinApis.success++;
                break;
            case 'api_error':
                joinApis.pending--;
                joinApis.failed++;
                break;

            case 'socket_error':
                return;
            case 'socket_disconnected':
                sockets--;
                disconnections++;
                break;
            case 'socket_connected':
                sockets++;
                break;
            case 'player_screen_question':
                if(!questions[payload.question.id]) {
                    questions[payload.question.id] = {asked: 1, answered: {}, failed: 0};
                    for(let i of payload.question.get_answers_by_question_id) {
                        questions[payload.question.id].answered[i.id] = 0;
                    }
                } else
                    questions[payload.question.id].asked++;
                logQuestion(payload.question.id);
                return;
            case 'player_answer_success':
                questions[payload.questionId].answered[payload.answerId]++;
                logQuestion(payload.questionId);
                return;
            case 'player_question_failed':
                questions[payload.questionId].failed++;
                logQuestion(payload.questionId);
                return;
            case 'update_team':
            default:
                return;
        }
        console.log(`BOTS ${bots} DISC ${disconnections} SOCKETS ${sockets} API ${joinApis.pending}|${joinApis.success}|${joinApis.failed}`)
    })

    setup(monitorListener);

    monitorListener.emit('start', teamIds.map(i => ({teamId: i, gameId, playersCount: playersPerTeam})))
};

start();

process.on('SIGINT', () => {
    console.log('\nstopping...')
    monitorListener?.emit('stop')
})
